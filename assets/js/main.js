$(document).ready(function(){
    new WOW().init();

    $(".toggle-menu").click(function(){
        $("#menu").slideToggle();
        return false;
    });

    if($("#slider").length > 0)
    {
        $('#slider').slick({
            dots: true,
            arrows:false
        });
    }

    if($("#galeria").length > 0)
    {
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            centerMode: true,
            focusOnSelect: true
        });
    }

    if($("#galeriav").length > 0)
    {
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
            asNavFor: '.slider-nav',
        });
        $('.slider-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            vertical: true
        });
    }
});